<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 19/11/18
 * Time: 01:22 PM
 */

namespace LaRecetta\Contact\Block\OfficeLocation;


use Magento\Framework\View\Element\Template;

class OfficeLocation extends \Magento\Framework\View\Element\Template
{
    protected $_collectionFactory;
    public function __construct(
        Template\Context $context,
        \LaRecetta\Contact\Model\ResourceModel\OfficeLocation\CollectionFactory $collectionFactory,
        array $data = []
    )
    {
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context, $data);
    }

    public function getOfficeLocations(){
        return $this->_collectionFactory->create()->getItems();
    }
}
