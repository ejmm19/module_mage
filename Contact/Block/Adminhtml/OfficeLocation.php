<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 14/11/18
 * Time: 10:22 AM
 */

namespace LaRecetta\Contact\Block\Adminhtml;


class OfficeLocation extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_officelocation';
        $this->_blockGroup = 'LaRecetta_Contact';
        $this->_headerText = __('Office Location');
        $this->_addButtonLabel = __('Agregar Ubicación');
        parent::_construct();
    }
}