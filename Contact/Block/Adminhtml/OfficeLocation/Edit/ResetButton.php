<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 17/11/18
 * Time: 08:00 AM
 */

namespace LaRecetta\Contact\Block\Adminhtml\OfficeLocation\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class ResetButton
 */
class ResetButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Reset'),
            'class' => 'reset',
            'on_click' => 'location.reload();',
            'sort_order' => 30
        ];
    }
}
