<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 17/11/18
 * Time: 08:01 AM
 */

namespace LaRecetta\Contact\Block\Adminhtml\OfficeLocation\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class SaveButton
 */
class SaveButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Save OfficeLocation'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
        ];
    }
}