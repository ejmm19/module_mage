<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 15/11/18
 * Time: 01:37 PM
 */

namespace LaRecetta\Contact\Block\Contact;


use Magento\Framework\View\Element\Template;
use LaRecetta\ConfigContent\Model\RequestTypeFactory;
use LaRecetta\ConfigContent\Model\RequestProcessFactory;

class ContactForm extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Customer\Model\Session $_customerSession
     */
    private $_customerSession;

    /**
     * @var \LaRecetta\ConfigContent\Model\RequestTypeFactory
     */
    private $_requestTypeFactory;

    /**
     * @var \LaRecetta\ConfigContent\Model\RequestProcessFactory
     */
    private $_requestProcessFactory;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */

    private $_resource;

    /**
     * @var \LaRecetta\ConfigContent\Model\DocumentTypes
     */
    private $_documentTypes;

    /**
     * @var \LaRecetta\ConfigContent\Model\Regionals
     */

    private $_regionals;
    /**
     * ContactForm constructor.
     * @param Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \LaRecetta\ConfigContent\Model\RequestProcessFactory $requestProcessFactory,
        \LaRecetta\ConfigContent\Model\RequestTypeFactory $requestTypeFactory,
        \LaRecetta\ConfigContent\Model\ResourceModel\DocumentTypes\CollectionFactory $documentTypesFactory,
        \LaRecetta\ConfigContent\Model\ResourceModel\Regionals\CollectionFactory $regionalsFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        array $data = []
    )
    {
        $this->_customerSession = $customerSession;
        $this->_requestTypeFactory = $requestTypeFactory;
        $this->_requestProcessFactory = $requestProcessFactory;
        $this->_documentTypes = $documentTypesFactory;
        $this->_regionals = $regionalsFactory;
        $this->_resource = $resource;
        parent::__construct($context, $data);
    }

    public function isLoggedIn(){
        return $this->_customerSession->isLoggedIn();
    }
    public function getRequestType()
    {
        $requestFactory = $this->_requestTypeFactory->create();
        $collection = $requestFactory->getCollection();
        return $collection->getData();
    }
    public function getRequestProcess()
    {

        $requestProcessCollection = $this->_requestProcessFactory->create()->getCollection();
        $requestProcessCollection->getSelect()->join(
            ['requestype' => $requestProcessCollection->getTable('pqr_request_type')],
            'main_table.pqr_request_type_id = requestype.entity_id', //'*'
            [
                'main_table.entity_id as pqr_request_process_id',
                'main_table.public as pqr_request_process_public',
                'requestype.entity_id as pqr_request_type_id',
                'requestype.description as pqr_request_type_name'
            ]
        )->join(
            ['processtype' => $requestProcessCollection->getTable('pqr_process')],
            'main_table.pqr_process_id = processtype.entity_id',
            [
                'processtype.entity_id as pqr_process_id',
                'processtype.name as pqr_process_name'
            ]
        )->join(
            ['formfield' => $requestProcessCollection->getTable('pqr_form')],
            'main_table.entity_id = formfield.pqr_request_process_id',
            [
                'formfield.pqr_form_field_id as pqr_form_field_id'
            ]
        );

        if(!$this->_customerSession->isLoggedIn()){
            $requestProcessCollection->getSelect()->where('main_table.public=1');
        }
        $resp = $requestProcessCollection->getData();
        $result = array();
        foreach ($resp as $value){
            if(!isset($result[$value['pqr_request_type_id']])) {
                $result[$value['pqr_request_type_id']] = array(
                    'pqr_request_type_id' => $value['pqr_request_type_id'],
                    'pqr_request_type_name' => $value['pqr_request_type_name'],
                    'Process' => array()
                );
            }
            if(!isset($result[$value['pqr_request_type_id']]['Process'][$value['pqr_process_id']])) {
                $result[$value['pqr_request_type_id']]['Process'][$value['pqr_process_id']] = array(
                    'pqr_request_process_id' => $value['entity_id'],
                    'pqr_process_id' => $value['pqr_process_id'],
                    'pqr_process_name' => $value['pqr_process_name'],
                    'Fields' => array()
                );
            }
            $result[$value['pqr_request_type_id']]['Process'][$value['pqr_process_id']]['Fields'][] = 'field_'.$value['pqr_form_field_id'];
        }

        return $result;
    }
    public function getDocumentTypes()
    {
        return $this->_documentTypes->create()->getData();
    }

    public function getRegionals()
    {
        return $this->_regionals->create()->getData();
    }

}
