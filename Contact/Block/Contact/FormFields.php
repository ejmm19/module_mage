<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 27/12/18
 * Time: 08:07 AM
 */

namespace LaRecetta\Contact\Block\Contact;


use Magento\Framework\View\Element\Template;
use LaRecetta\ConfigContent\Model\ResourceModel\FormFields\Collection as CollectionFormFields;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\CustomerFactory;
use Cminds\MultiUserAccounts\Model\ResourceModel\Subaccount\CollectionFactory as SubaccountCollectionFactory;

class FormFields extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \LaRecetta\ConfigContent\Model\ResourceModel\FormFields\Collection  $_collectionFormFields
     */
    protected $_collectionFormFields;

    protected $_customer;

    protected $_customerFactory;

    protected $_subAccounts;

    public function __construct(
        CollectionFormFields $collectionFormFields,
        Session $customer,
        CustomerFactory $customerFactory,
        SubaccountCollectionFactory $subAccounts,
        Template\Context $context,
        array $data = [])
    {
        $this->_collectionFormFields = $collectionFormFields;
        $this->_customer = $customer;
        $this->_customerFactory = $customerFactory;
        $this->_subAccounts = $subAccounts;
        parent::__construct($context, $data);
    }

    public function getFormsFields()
    {
        return $this->_collectionFormFields->getData();
    }
    public function getCustomer()
    {
        return $this->_customer->getCustomer();
    }
    public function getParentId()
    {
        return $this->_customer->getParentId();
    }
    public function getModelCustomer($id)
    {
        return $this->_customerFactory->create()->load($id);
    }
    public function getSubAccounts($id)
    {
        $subAccounts = $this->_subAccounts->create()->filterByParentCustomerId($id)->addFieldToSelect('customer_id');
        $dataChilds = $subAccounts->getData();
        $childsIds = array();
        foreach ($dataChilds as $item){
            $childsCustomer = $this->_customerFactory->create()->load($item['customer_id']);
            $address = $this->getAddress($childsCustomer);
            $childsIds[$item['customer_id']] = array(
                "direction" => $address,
                "data" => array(
                    "nit" => $childsCustomer->getNit(),
                    "sap_code" => $childsCustomer->getSapCode(),
                    "business_name" => $childsCustomer->getBusinessName()
                )
            );
        }
        return $childsIds;
    }

    /**
     * @param \Magento\Customer\Model\Customer $customer
     * @return |null
     */
    public function getAddress($customer)
    {
        $customerObj = $customer;
        $fullAddress = array();
        if ($customerObj) {
            $customerAddress = array();
            foreach ($customerObj->getAddresses() as $address) {
                $street = $address->getStreet();
                $customerAddress['city'] = !empty($address->getCity()) ? $address->getCity() : '';
                $customerAddress['street'] = isset($street[0]) ? $street[0] : '';
                $customerAddress['district'] = isset($street[1]) ? $street[1] : '';
            }

            if (count($customerAddress)) {
                $fullAddress['dir'] = $customerAddress['street'];
                $fullAddress['dir'] .= !empty($customerAddress['district']) ?
                    (!empty($fullAddress['dir']) ? ', ' : '') .$customerAddress['district'] : '';
                $fullAddress['city'] = !empty($customerAddress['city']) ? $customerAddress['city'] : '';
            }
        }
        return $fullAddress;
    }

}