<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 15/11/18
 * Time: 01:22 PM
 */

namespace LaRecetta\Contact\Controller\Contact;


use Magento\Framework\App\Action\Context;

class ContactForm extends \Magento\Framework\App\Action\Action
{

    protected $_pageFactory;

    protected $_resultPage;

    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $this->_resultPage = $this->_pageFactory->create();
        $this->_resultPage->getConfig()->getTitle()->set((__('Formulario de Contacto')));

        return $this->_resultPage;
    }
}
