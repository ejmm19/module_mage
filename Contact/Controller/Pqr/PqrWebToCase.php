<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 3/01/19
 * Time: 04:55 PM
 */

namespace LaRecetta\Contact\Controller\Pqr;


use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use LaRecetta\RestClient\Helper\RestClient;
use Magento\Framework\Data\Form\FormKey\Validator;
use LaRecetta\Customer\Model\Customer;

class PqrWebToCase extends \Magento\Framework\App\Action\Action
{
    /**
     * @var JsonFactory
     */
    protected $_jsonFactory;

    /**
     * @var RestClient
     */
    protected $_restClient;

    /**
     * @var Validator
     */
    protected $_formKeyValidator;

    /**
     * @var Customer
     */

    private $_customerFactory;



    const SERVICE_GET_TOKEN_BY_SUGAR = 'sugar_token';

    const SERVICE_WEB_TO_CASE = 'web_to_case';

    public function __construct(
        JsonFactory $jsonFactory,
        Validator $formKeyValidator,
        RestClient $restClient,
        Customer $customerMageFactory,
        Context $context
    )
    {
        $this->_jsonFactory = $jsonFactory;
        $this->_formKeyValidator = $formKeyValidator;
        $this->_restClient = $restClient;
        $this->_customerFactory = $customerMageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        if ($this->_request->isAjax()):

            try{
            //get Token By Sugar
            $responseServiceToken = $this->_restClient->getResponseByService(self::SERVICE_GET_TOKEN_BY_SUGAR, null);
            $responseToken = json_decode($responseServiceToken, true);

            //Validate Token
            if (empty($responseToken['error'])):
                if (empty($responseToken['access_token'])):
                    throw new \Exception('En este momento el sistema no se encuentra disponible. Por favor intente más tarde.');
                else:
                    $token = $responseToken['access_token'];
                endif;
            else:
                throw new \Exception('Ha ocurrido un error al obtener el Token - '.$responseToken['error_message']);
            endif;

            /*Get Customer Logued*/
            //set to data Before send
            $customerid = $this->_request->getParam('customerId');
            $sap_code_customer = $this->_request->getParam('sap_code_customer');
            $nit = $this->_request->getParam('nit');
            $email = $this->_request->getParam('email');
            $firstname = $this->_request->getParam('firstname');
            $lastname = $this->_request->getParam('lastname');
            $celular = $this->_request->getParam('telephone');
            $sasa_tificacion_casos_cases_1sasa_tificacion_casos_ida = '0b59b2fc-8a88-11e6-8d46-06b20b8677ed';
            $url_request = $this->_request->getParam('url_request');
            $address = $this->_request->getParam('address');


            $description_req = !empty($this->_request->getParam('description_req')) ? 'Descripción: '.$this->_request->getParam('description_req'):'';
            $request =   !empty($this->_request->getParam('request')) ? 'Requerimiento: '.$this->_request->getParam('request'):'';
            $process =   !empty($this->_request->getParam('process')) ? 'Asunto: '.$this->_request->getParam('process'):'';
            $product_name =   !empty($this->_request->getParam('product_name')) ? 'Nombre del producto: '.$this->_request->getParam('product_name'):'';
            $sku =   !empty($this->_request->getParam('sku')) ? 'Nombre del producto: '.$this->_request->getParam('sku'):'';
            $cant_prod =   !empty($this->_request->getParam('cant_prod')) ? 'Cantidad del producto con novedad: '.$this->_request->getParam('cant_prod'):'';
            $no_factura =   !empty($this->_request->getParam('no_factura')) ? 'Número de factura: '.$this->_request->getParam('no_factura'):'';

            $motivo_contacto = $description_req." - ".$request." - ".$process." - ".$product_name." - ".$no_factura." - ".$sku." - ".$cant_prod;

            if (!empty($customerid) && !empty($sap_code_customer)):
                $customerBySap = $this->_customerFactory->getCustomerBySapCode($sap_code_customer);
                $sasa_abreviaturasociedad_c = $customerBySap['sales_org'];
                $distr_channel = $customerBySap['distr_channel'];
                $division = $customerBySap['division'];
                $sasa_sociedad_por_cuenta = "$sap_code_customer-AGLR-$sasa_abreviaturasociedad_c-$distr_channel-$division";
                $account_id = $customerBySap['sap_code'];
                $sasa_relacion_c =  1;
                $acepto_habeas = "Si";
            else:
                $sasa_sociedad_por_cuenta = 'de52d440-f09f-11e6-8e4e-06eb7fcc0457';
                $sasa_abreviaturasociedad_c = 'AGLR';
                $sasa_relacion_c =  9;
                $account_id = "e2dd17aa-8984-11e6-aaf9-06d48441b777";
                $acepto_habeas = !empty($this->_request->getParam('terms_conds')) && $this->_request->getParam('terms_conds') === 'on' ? 'Si' : 'No';
            endif;
            //Set data description

                $this->_restClient->setHeaders(
                    array(
                        'oauth-token: '.$token,
                        'cache-control: no-cache',
                        'Content-Type: application/json'
                    )
                );

                $dataSevicePqr = array(
                    "fields" => array(
                        "sasa_abreviaturasociedad_c" => $sasa_abreviaturasociedad_c,
                        "sasa_sociedad_por_cuenta" => "$sasa_sociedad_por_cuenta",
                        "account_id" => "$account_id",
                        "sasa_relacion_c" => $sasa_relacion_c,
                        "sasa_fuente_c" => 9,
                        "sasa_tificacion_casos_cases_1sasa_tificacion_casos_ida" => $sasa_tificacion_casos_cases_1sasa_tificacion_casos_ida,
                        "type" => "nodrizzaLaRecetta"
//                        "sasalr_numerofactura_c" => $no_factura
                    ),
                    "description" => array(
                        "nombres" => $firstname,
                        "apellidos" => $lastname,
                        "correo_electronico" => $email,
                        "numero_documento" => $nit,
                        "numero_celular" => $celular,
                        "direccion" => $address,
                        "motivo_contacto" => $motivo_contacto,
                        "acepto_habeas" => $acepto_habeas,
                        "tipo_formulario" => "Formulario Pqr",
                        "url" => $url_request,
                        "agencia" => "1068060"
                    )
                );
                $dataSevicePqr = json_encode($dataSevicePqr);


                $postPqr = $this->_restClient->getResponseByService(
                    self::SERVICE_WEB_TO_CASE,
                    $dataSevicePqr
                );

                $postPqr = json_decode($postPqr,true);

                if (empty($postPqr) || empty($postPqr['response']['status']) ||$postPqr['response']['status'] != 'OK'){
                    throw new \Exception('En este momento el sistema no se encuentra disponible. Por favor intente más tarde.');
                }

                $dataresponse = $postPqr;
                $status = 'Ok';
                $title = 'Solicitud Enviada';
                $msj = "Su solicitud ha sido recibida de forma correcta con el caso No. ".$postPqr['case_number']." y será gestionado con el área encargada.";

            }catch(\Exception $e){
                $dataresponse = '';
                $status = 'Error';
                $title = 'Falla del Sistema';
                $msj = $e->getMessage();
            }
            $response = [
                'response' => $dataresponse,
                'status' => $status,
                'title' => $title,
                'message' => $msj
            ];
            return $this->_jsonFactory->create()->setData($response);
        else:
            $this->_redirect('/');
        endif;
    }
}