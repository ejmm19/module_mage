<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 7/01/19
 * Time: 01:01 PM
 */

namespace LaRecetta\Contact\Controller\Pqr;


use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use LaRecetta\RestClient\Helper\RestClient;
use Magento\Framework\Data\Form\FormKey\Validator;

class Iclient extends \Magento\Framework\App\Action\Action
{
    /**
     * @var JsonFactory
     */
    protected $_jsonFactory;

    /**
     * @var RestClient
     */
    protected $_restClient;

    /**
     * @var Validator
     */

    protected $_formKeyValidator;

    const SERVICE_GET_TOKEN_BY_SUGAR = 'sugar_token';

    const SERVICE_WEB_TO_LEAD = 'web_to_lead';

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        RestClient $restClient
    )
    {
        parent::__construct($context);
        $this->_jsonFactory = $jsonFactory;
        $this->_restClient = $restClient;
    }

    public function execute()
    {
        if ($this->_request->IsAjax()):
            try{

                //Validate Params
                if (!$this->_request->getParam('firstname') && !$this->_request->getParam('lastname') && !$this->_request->getParam('telephone') &&
                    !$this->_request->getParam('email') && !$this->_request->getParam('ic_terms_conds') && !$this->_request->getParam('details') ):
                    throw new \Exception('Faltan datos que son obligatorios');
                endif;

                //get Token
                $responseServiceToken = $this->_restClient->getResponseByService(self::SERVICE_GET_TOKEN_BY_SUGAR, null);
                $responseToken = json_decode($responseServiceToken, true);

                //Validate token in response
                if (empty($responseToken['error'])) {
                    $token = $responseToken['access_token'];
                }else {
                    throw new \Exception('En este momento el sistema no se encuentra disponible. Por favor intente más tarde.');
                }

                //Set a Data Before Send
                $this->_restClient->setHeaders(
                    array(
                        'oauth-token: '.$token,
                        'cache-control: no-cache',
                        'Content-Type: application/json'
                    )
                );
                $dataWebToLead = array(
                    "sociedad" => "AGLR",
                    "campaign_id" => "05c1ad7c-eddc-11e8-a6a5-06a2c62711a2",
                    "agencia" => "1068060",
                    "lead_source" => "Web Site",
                    "url" => $this->_request->getParam('url_request'),
                    "first_name" => $this->_request->getParam('firstname'),
                    "last_name" => $this->_request->getParam('lastname'),
                    "description" => $this->_request->getParam('details'),
                    "primary_address_country" => "Colombia",
                    "account_name" => $this->_request->getParam('bussinessname'),
                    "email1" => $this->_request->getParam('email'),
                    "phone_mobile" => $this->_request->getParam('telephone'),
                    "sasa_aceptacondiciones_c" => 1,
                    "sasa_tipodocumento_c" => intval($this->_request->getParam('type_id')),
                    "sasa_nrodocumento_c" => $this->_request->getParam('identification'),
                    "sasa_regional_c" => trim($this->_request->getParam('regional')),
                    "team_id" => "9e0f2c48-8b1f-11e6-8ad6-06d48441b777"
                );

                $dataWebToLead = json_encode($dataWebToLead);

                $postWebToLead = $this->_restClient->getResponseByService(self::SERVICE_WEB_TO_LEAD,$dataWebToLead);

                $postWebToLead = json_decode($postWebToLead,true);

                //Set message to show in view
                if (empty($postWebToLead['name']) && empty($postWebToLead['response']['id'])):
                    $title = 'Datos Existentes';
                    $msj = "Los datos ingresados corresponden a unos datos ya registrados en nuestro sistema.";
                    $status = 'Used';
                else:
                    $title = 'Solicitud Enviada';
                    $msj = "Muchas gracias por contactarse con nosotros. Su solicitud ha sido enviada exitosamente. Uno de nuestros asesores se comunicará pronto con usted.";
                    $status = 'Ok';
                endif;

                $dataresponse = $postWebToLead;

            }catch (\Exception $e){
                $dataresponse = '';
                $title = 'Falla del Sistema';
                $status = 'Error';
                $msj = $e->getMessage();
            }
            $response = [
                'response' => $dataresponse,
                'status' => $status,
                'title' => $title,
                'message' => $msj
            ];
            return $this->_jsonFactory->create()->setData($response);
        endif;
    }

}
