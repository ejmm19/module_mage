<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 26/12/18
 * Time: 09:30 AM
 */

namespace LaRecetta\Contact\Controller\Pqr;


use Magento\Framework\Controller\Result\JsonFactory;
use LaRecetta\ConfigContent\Model\ProcessFactory;
use LaRecetta\ConfigContent\Model\RequestProcessFactory;

class SelectPqr extends \Magento\Framework\App\Action\Action
{
    protected $_jsonFactory;

    protected $_processFactory;

    protected $_requestProcessFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        ProcessFactory $processFactory,
        RequestProcessFactory $requestProcessFactory,
        JsonFactory $jsonFactory
    )
    {
        $this->_jsonFactory = $jsonFactory;
        $this->_processFactory = $processFactory;
        $this->_requestProcessFactory = $requestProcessFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        if ($this->_request->IsAjax()) :
            $result = $this->_jsonFactory->create();
            $responseAjax =  $this->getProcess();
            return $result->setData($responseAjax);
        else:
            $this->_redirect('/');
        endif;
    }

}