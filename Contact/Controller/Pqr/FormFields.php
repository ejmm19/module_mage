<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 3/01/19
 * Time: 08:56 AM
 */

namespace LaRecetta\Contact\Controller\Pqr;


use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use LaRecetta\ConfigContent\Model\RequestProcessFactory;
use LaRecetta\ConfigContent\Model\PqrFormFactory;

class FormFields extends \Magento\Framework\App\Action\Action
{
    protected $_jsonFactory;

    protected $_formFieldFactory;

    protected $_requestProcess;

    protected $_pqrFormFactory;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        RequestProcessFactory $requestProcess,
        PqrFormFactory $pqrFormFactory
    )
    {
        $this->_jsonFactory = $jsonFactory;
        $this->_requestProcess = $requestProcess;
        $this->_pqrFormFactory = $pqrFormFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $request = $this->_request->getParam('request');
        $process = $this->_request->getParam('process');
        $requestProcess = $this->_requestProcess->create();
        $requestProcessId = $requestProcess->getCollection()
            ->addFieldToFilter('pqr_request_type_id',['eq' => trim($request)])
            ->addFieldToFilter('pqr_process_id',['eq' => trim($process)])
            ->addFieldToSelect('entity_id');
        $pqrFormId = $requestProcessId->getData();
        $pqrForm = $this->_pqrFormFactory->create();
        $formFields = $pqrForm->getCollection()
            ->addFieldToFilter('pqr_request_process_id',['eq' => $pqrFormId])
            ->addFieldToSelect('pqr_form_field_id');
        $fields = array();
        foreach ($formFields as $formField) {
            $fields[] = $formField['pqr_form_field_id'];
        }
        return $this->_jsonFactory->create()->setData(['formfields' => $fields]);
    }
}