<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 16/11/18
 * Time: 11:07 AM
 */

namespace LaRecetta\Contact\Controller\Adminhtml\OfficeLocation;


class Index extends \Magento\Backend\App\Action
{
    /**
     * Index action
     *
     * @return void
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}