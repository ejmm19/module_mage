<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 17/11/18
 * Time: 09:44 AM
 */

namespace LaRecetta\Contact\Controller\Adminhtml\OfficeLocation;

use LaRecetta\Contact\Model\OfficeLocation as OfficeLocation;

class MassDelete extends \Magento\Backend\App\Action
{
    public function execute()
    {
        try {
            $ids = $this->getRequest()->getParam('selected', []);
            if (!is_array($ids) || !count($ids)) {
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/index', array('_current' => true));
            }
            foreach ($ids as $id) {
                if ($OfficeLocation = $this->_objectManager->create(OfficeLocation::class)->load($id)) {
                    $OfficeLocation->delete();
                }
            }
            $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', count($ids)));

            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }catch (\Exception $e){
            $this->messageManager->addException(
                $e,
                __('Algo salió mal mientras se borraban las ubicaciones. ')
            );
        }
    }
}