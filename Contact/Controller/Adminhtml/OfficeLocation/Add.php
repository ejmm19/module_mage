<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 16/11/18
 * Time: 06:01 PM
 */

namespace LaRecetta\Contact\Controller\Adminhtml\OfficeLocation;

use LaRecetta\Contact\Model\OfficeLocation as OfficeLocation;

class Add extends \Magento\Backend\App\Action
{
    /**
     * Add A OfficeLocation Page
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        try {
            $this->_view->loadLayout();
            $this->_view->renderLayout();

            $OfficeLocationDatas = $this->getRequest()->getParam('OfficeLocation');

            if (is_array($OfficeLocationDatas)) {
                $OfficeLocation = $this->_objectManager->create(OfficeLocation::class);
                $OfficeLocation->setData($OfficeLocationDatas)->save();
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/index');
            }
        }catch (\Exception $e){
            $this->messageManager->addException(
                $e,
                __('Algo salió mal mientras se creaba la ubicación. ')
            );
        }
    }
}