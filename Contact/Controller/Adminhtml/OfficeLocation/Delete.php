<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 17/11/18
 * Time: 08:02 AM
 */

namespace LaRecetta\Contact\Controller\Adminhtml\OfficeLocation;

use LaRecetta\Contact\Model\OfficeLocation as OfficeLocation;

class Delete extends \Magento\Backend\App\Action
{
    public function execute()
    {
        try {
            $id = $this->getRequest()->getParam('id');

            if (!($OfficeLocation = $this->_objectManager->create(OfficeLocation::class)->load($id))) {
                $this->messageManager->addError(__('Unable to proceed. Please, try again.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/index', array('_current' => true));
            }
            try {
                $OfficeLocation->delete();
                $this->messageManager->addSuccess(__('Your OfficeLocation item has been deleted !'));
            } catch (Exception $e) {
                $this->messageManager->addError(__('Error while trying to delete OfficeLocation: '));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/index', array('_current' => true));
            }

            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }catch (\Exception $e){
            $this->messageManager->addException(
                $e,
                __('Algo salió mal mientras se borraba la ubicación. ')
            );
        }
    }
}