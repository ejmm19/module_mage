<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 30/01/19
 * Time: 10:01 AM
 */

namespace LaRecetta\Contact\Test\Unit\Block\Contact;

use LaRecetta\Contact\Block\Contact\FormFields;
use Magento\Framework\View\Element\Template\Context;
use LaRecetta\ConfigContent\Model\ResourceModel\FormFields\Collection as CollectionFormFields;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\CustomerFactory;
use PHPUnit\Framework\TestCase;

class FormFieldsTest extends TestCase
{
    /**
     * @var FormFields
     */
    protected $object;

    /**
     * @var Context
     */
    protected $context;

    /**
     * @var CollectionFormFields
     */

    protected $collectionFormField;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $subaccountCollectionFactory;


    /**
     * @codeCoverageIgnore
     */

    public function setUp()
    {
        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $this->context = $this->getMockBuilder(\Context::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->collectionFormField = $this->getMockBuilder(\LaRecetta\ConfigContent\Model\ResourceModel\FormFields\Collection::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->session = $this->getMockBuilder(\Magento\Customer\Model\Session::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->customerFactory = $this->getMockBuilder(\Magento\Customer\Model\CustomerFactory::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->subaccountCollectionFactory = $this->getMockBuilder(\Cminds\MultiUserAccounts\Model\ResourceModel\Subaccount\CollectionFactory::class)
            ->disableOriginalConstructor()
            ->getMock();


        $this->object = $objectManager->getObject(\LaRecetta\Contact\Block\Contact\FormFields::class,[
            'SubaccountCollectionFactory' => $this->subaccountCollectionFactory
        ]);
    }

    public function tearDown()
    {
        $this->object = null;
    }


    /**
     * @covers \LaRecetta\Contact\Block\Contact\FormFields::getParentId
     */
    public function testGetParentId()
    {
        $this->assertEquals(null,$this->object->getParentId());
    }

    /**
     * @covers \LaRecetta\Contact\Block\Contact\FormFields::getCustomer
     */
    public function testGetCustomer()
    {
        $this->assertEquals(null,$this->object->getCustomer());
    }

    /**
     * @covers \LaRecetta\Contact\Block\Contact\FormFields::getSubAccounts
     */
    public function testGetSubAccounts()
    {
        /*$result = $this->object->getSubAccounts(6);
        $expected = 10;*/
        $this->assertInstanceOf(\LaRecetta\Contact\Block\Contact\FormFields::class,$this->object);
    }
}
