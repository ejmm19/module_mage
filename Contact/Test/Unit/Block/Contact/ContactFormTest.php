<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 29/01/19
 * Time: 10:05 AM
 */

namespace LaRecetta\Contact\Test\Unit\Block\Contact;

use LaRecetta\Contact\Block\Contact\ContactForm;
use PHPUnit\Framework\TestCase;

class ContactFormTest extends TestCase
{

    /**
     * @var ContactForm
     */

    protected $object;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */

    protected $context;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */

    private $_customerSession;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */

    private $_requestTypeFactory;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */

    private $_requestProcessFactory;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */

    private $_resource;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */

    private $_documentTypes;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */

    private $_regionals;


    /**
     * @codeCoverageIgnore
     */

    public function setUp()
    {
        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->context = $this->getMockBuilder(\Magento\Framework\View\Element\Template\Context::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->_customerSession = $this->createMock(\Magento\Customer\Model\Session::class);
        $this->_requestTypeFactory = $this->getMockBuilder(\LaRecetta\ConfigContent\Model\RequestTypeFactory::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->_requestProcessFactory = $this->getMockBuilder(\LaRecetta\ConfigContent\Model\RequestProcessFactory::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->_resource = $this->createMock(\Magento\Framework\App\ResourceConnection::class);
        $this->_documentTypes = $this->getMockBuilder(\LaRecetta\ConfigContent\Model\ResourceModel\DocumentTypes\CollectionFactory::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->_regionals = $this->getMockBuilder(\LaRecetta\ConfigContent\Model\ResourceModel\Regionals\CollectionFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->object = $objectManager->getObject(\LaRecetta\Contact\Block\Contact\ContactForm::class,
            [
                'Session' => $this->_customerSession,
                'RequestProcess' => $this->_requestProcessFactory,
                'RequestTypeFactory' => $this->_requestTypeFactory,
                'DocumentTypes' => $this->_documentTypes,
                'Regionals' => $this->_regionals,
                'ResourceConnection' => $this->_resource
            ]);
    }
    /**
     * @codeCoverageIgnore
     */
    public function tearDown()
    {
        $this->object = null;
    }

    public function testGetInstance()
    {
        $this->assertInstanceOf(ContactForm::class,$this->object);
    }

}
