<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 29/01/19
 * Time: 02:33 PM
 */

namespace LaRecetta\Contact\Test\Unit\Block\OfficeLocation;

use PHPUnit\Framework\TestCase;

class OfficeLocationTest extends TestCase
{

    /**
     * @var \LaRecetta\Contact\Block\OfficeLocation\OfficeLocation
     */

    protected $block;

    /**
    * @var \PHPUnit_Framework_MockObject_MockObject
    */

    protected $collectionOffice;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */

    protected $context;


    /**
     * @codeCoverageIgnore
     */

    function setUp()
    {

        $this->context = $this->getMockBuilder('Magento\Backend\Block\Template\Context')
            ->disableOriginalConstructor()
            ->getMock();
        $this->collectionOffice = $this->getMockBuilder('\LaRecetta\Contact\Model\ResourceModel\OfficeLocation\CollectionFactory')
            ->disableOriginalConstructor()
            ->getMock();


        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->block = $objectManager->getObject(\LaRecetta\Contact\Block\OfficeLocation\OfficeLocation::class,
            [
                "context" => $this->context,
                "collectionFactory" => $this->collectionOffice
            ]
            );
    }

    protected function tearDown()
    {
        $this->block = null;
    }


    public function testGetOfficeLocations()
    {
        $this->assertInstanceOf(\LaRecetta\Contact\Block\OfficeLocation\OfficeLocation::class,$this->block);
    }
}
