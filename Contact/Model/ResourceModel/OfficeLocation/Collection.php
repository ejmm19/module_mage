<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 14/11/18
 * Time: 08:56 AM
 */

namespace LaRecetta\Contact\Model\ResourceModel\OfficeLocation;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    protected $_idFieldName = 'entity_id';

    protected function _construct()
    {
        $this->_init('LaRecetta\Contact\Model\OfficeLocation','LaRecetta\Contact\Model\ResourceModel\OfficeLocation');
    }
}