<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 13/11/18
 * Time: 05:25 PM
 */

namespace LaRecetta\Contact\Model\ResourceModel;


class OfficeLocation extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected $_idFieldName = 'entity_id';

    protected function _construct()
    {
        $this->_init('lr_office_location','entity_id');
    }
}