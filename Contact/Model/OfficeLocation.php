<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 13/11/18
 * Time: 05:21 PM
 */

namespace LaRecetta\Contact\Model;


use Magento\Framework\Model\AbstractModel;

/**
 * OfficeLocation Model
 *
 */
class OfficeLocation extends AbstractModel
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime
     */
    protected $_dateTime;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\LaRecetta\Contact\Model\ResourceModel\OfficeLocation::class);
    }

}