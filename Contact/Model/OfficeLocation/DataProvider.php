<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 17/11/18
 * Time: 07:47 AM
 */

namespace LaRecetta\Contact\Model\OfficeLocation;

use LaRecetta\Contact\Model\ResourceModel\OfficeLocation\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $officeLocationCollectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $officeLocationCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $officeLocationCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();
        $this->loadedData = array();
        /** @var Customer $customer */
        foreach ($items as $contact) {
            // notre fieldset s'apelle "contact" d'ou ce tableau pour que magento puisse retrouver ses datas :
            $this->loadedData[$contact->getId()]['OfficeLocation'] = $contact->getData();
        }

        return $this->loadedData;

    }
}