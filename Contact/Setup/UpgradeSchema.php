<?php
/**
 * Created by PhpStorm.
 * User: grupodot
 * Date: 13/11/18
 * Time: 03:41 PM
 */

namespace LaRecetta\Contact\Setup;


use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Upgrade the Contact module DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if(version_compare($context->getVersion(), '0.0.3', '<')) {
            $this->createOfficeLocationTable($installer);
        }

        if(version_compare($context->getVersion(), '0.0.4', '<')) {
            $this->createPQRIndependientsTables($installer);
            $this->createPQRDependientsTables($installer);
        }

        $installer->endSetup();
    }

    public function createOfficeLocationTable(SchemaSetupInterface $installer)
    {
        /**
         * Create table 'lr_office_location'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('lr_office_location'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Entity Id'
            )
            ->addColumn(
                'name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Office name'
            )
            ->addColumn(
                'latitude',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                20,
                ['nullable' => false],
                'Office latitude'
            )
            ->addColumn(
                'longitude',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                20,
                ['nullable' => false],
                'Office longitude'
            )
            ->setComment('office location');

        $installer->getConnection()->createTable($table);
    }


    protected function createPQRIndependientsTables(SchemaSetupInterface $installer)
    {
        /**
         * Create table 'pqr_request_type'
         */
        $table1 = $installer->getConnection()
            ->newTable($installer->getTable('pqr_request_type'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                11,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID request type'
            )
            ->addColumn(
                'description',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                200,
                ['nullable' => false],
                'description request type'
            )
            ->setComment("pqr request type");
        $installer->getConnection()->createTable($table1);

        /**
         * Create table 'pqr_process'
         */
        $table2 = $installer->getConnection()
            ->newTable($installer->getTable('pqr_process'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                11,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID pqr process'
            )
            ->addColumn(
                'name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                200,
                ['nullable' => false],
                'Name pqr process'
            )
            ->setComment("pqr process");
        $installer->getConnection()->createTable($table2);

        /**
         * Create table 'pqr_form_field'
         */
        $table3 = $installer->getConnection()
            ->newTable($installer->getTable('pqr_form_field'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                11,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID pqr form field'
            )
            ->addColumn(
                'code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                100,
                ['nullable' => false],
                'pqr form field code'
            )
            ->addColumn(
                'label',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'pqr form field label'
            )
            ->addColumn(
                'public_rule',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                100,
                ['nullable' => false],
                'pqr form field public rule'
            )
            ->addColumn(
                'private_rule',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                100,
                ['nullable' => false],
                'pqr form field private rule'
            )
            ->addColumn(
                'type',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                100,
                ['nullable' => false],
                'pqr form field type'
            )
            ->setComment("pqr form field");
        $installer->getConnection()->createTable($table3);
    }

    protected function createPQRDependientsTables(SchemaSetupInterface $installer)
    {
        /**
         * Create table 'pqr_request_process'
         */
        $table4 = $installer->getConnection()
            ->newTable($installer->getTable('pqr_request_process'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                11,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID pqr request - process'
            )
            ->addColumn(
                'pqr_request_type_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                11,
                ['nullable' => false, 'unsigned' => true],
                'pqr request type id'
            )
            ->addColumn(
                'pqr_process_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                11,
                ['nullable' => false, 'unsigned' => true],
                'pqr process id'
            )
            ->addColumn(
                'public',
                \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                null,
                ['nullable' => false, 'default' => 0],
                'pqr public process'
            )
            ->addForeignKey(
                $installer->getFkName(
                    'pqr_request_process',
                    'pqr_request_type_id',
                    'pqr_request_type',
                    'entity_id'
                ),
                'pqr_request_type_id',
                $installer->getTable('pqr_request_type'),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )
            ->addForeignKey(
                $installer->getFkName(
                    'pqr_request_process',
                    'pqr_process_id',
                    'pqr_process',
                    'entity_id'
                ),
                'pqr_process_id',
                $installer->getTable('pqr_process'),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )
            ->addIndex(
                $installer->getIdxName(
                    $installer->getTable('pqr_request_process'),
                    ['pqr_request_type_id', 'pqr_process_id'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                ['pqr_request_type_id', 'pqr_process_id'],
                ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
            )
            ->setComment("pqr request process");
        $installer->getConnection()->createTable($table4);


        /**
         * Create table 'pqr_form'
         */
        $table5 = $installer->getConnection()
            ->newTable($installer->getTable('pqr_form'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                11,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID pqr form field'
            )
            ->addColumn(
                'pqr_request_process_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                11,
                ['nullable' => false, 'unsigned' => true],
                'pqr request process id'
            )
            ->addColumn(
                'pqr_form_field_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                11,
                ['nullable' => false, 'unsigned' => true],
                'pqr form field id'
            )
            ->addForeignKey(
                $installer->getFkName(
                    'pqr_form',
                    'pqr_request_process_id',
                    'pqr_request_process',
                    'entity_id'
                ),
                'pqr_request_process_id',
                $installer->getTable('pqr_request_process'),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )
            ->addForeignKey(
                $installer->getFkName(
                    'pqr_form',
                    'pqr_form_field_id',
                    'pqr_form_field',
                    'entity_id'
                ),
                'pqr_form_field_id',
                $installer->getTable('pqr_form_field'),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )
            ->addIndex(
                $installer->getIdxName(
                    $installer->getTable('pqr_form'),
                    ['pqr_request_process_id', 'pqr_form_field_id'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                ['pqr_request_process_id', 'pqr_form_field_id'],
                ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
            )
            ->setComment("pqr form");
        $installer->getConnection()->createTable($table5);

    }
}
